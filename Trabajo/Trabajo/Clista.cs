﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabajo
{
    
 public class CLista
  
    {

        private CNodo cabeza;

        public CLista()
        {
            cabeza = null;
        }

        public void InsertarDato(int dat)
        {
            CNodo antes, luego;
            CNodo NuevoNodo = new CNodo();
            NuevoNodo.Dato = dat;
            int ban = 0;
            if (cabeza == null)
            { //lista esta vacia
                NuevoNodo.Siguiente = null;
                cabeza = NuevoNodo;
                //cabeza.siguiente=null;
            }

            else
            {
                if (dat < cabeza.Dato)
                {//dato va antes de cabeza
                    NuevoNodo.Siguiente = cabeza;
                    cabeza = NuevoNodo;
                }
                else
                {
                    antes = cabeza;
                    luego = cabeza;
                    while (ban == 0)
                    {
                        if (dat >= luego.Dato)
                        {
                            antes = luego;
                            luego = luego.Siguiente;
                        }
                        if (luego == null)
                        {
                            ban = 1;
                        }
                        else
                        {
                            if (dat < luego.Dato)
                            {
                                ban = 1;
                            }
                        }
                    }
                    antes.Siguiente = NuevoNodo;
                    NuevoNodo.Siguiente = luego;
                }
            }
        }

        public void EliminarDato(int dat)
        {
            CNodo antes, luego;
            int ban = 0;
            if (Vacia())
            {
                Console.Write("Lista vacía ");
            }
            else
            {
                if (dat < cabeza.Dato)
                {
                    Console.Write("dato no existe en la lista ");
                }
                else
                {
                    if (dat == cabeza.Dato)
                    {
                        cabeza = cabeza.Siguiente;
                    }
                    else
                    {
                        antes = cabeza;
                        luego = cabeza;
                        while (ban == 0)
                        {
                            if (dat > luego.Dato)
                            {
                                antes = luego;
                                luego = luego.Siguiente;
                            }
                            else ban = 1;
                            if (luego == null)
                            {
                                ban = 1;
                            }
                            else
                            {
                                if (luego.Dato == dat)
                                    ban = 1;
                            }
                        }
                        if (luego == null)
                        {
                            Console.Write("dato no existe en la Lista ");
                        }
                        else
                        {
                            if (dat == luego.Dato)
                            {
                                antes.Siguiente = luego.Siguiente;
                            }
                            else
                                Console.Write("dato no existe en la Lista ");
                        }
                    }
                }
            }
        }

        public bool Vacia()
        {
            return (cabeza == null);
        }


        //  )

        public void VisualizarDatos()
        {
            CNodo Temporal;
            Temporal = cabeza;
            if (!Vacia())
            {
                while (Temporal != null)
                {
                    Console.Write(" " + Temporal.Dato + " ");
                    Temporal = Temporal.Siguiente;
                }
                Console.WriteLine("");
            }
            else
                Console.WriteLine("Lista vacía");
        }





    }

}